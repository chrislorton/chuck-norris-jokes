import React, { useState } from 'react';
import axios from 'axios';

export default function Search() {

    const [firstName, setActiveFirstName] = useState(null);
    const [lastName, setActiveLastName] = useState(null);
    const [showErrors, setShowErrors] = useState(false);
    const [activeSearchJoke, setActiveSearchJoke] = useState(null);

    const handleSearch = (e) => {
        e.preventDefault()
        if(!firstName || !lastName){
            setShowErrors(true)
        } else {
            setShowErrors(false)
            let searchString = `firstName=${firstName}&lastName=${lastName}`
            axios.get(`http://api.icndb.com/jokes/random?exclude=[explicit]&escape=javascript&${searchString}`)
            .then(res => {
                setActiveSearchJoke(res.data.value.joke)
            })
        }
    }

    const handleFirstNameUpdate = (e) => {
        setActiveFirstName(e.currentTarget.value)
    }

    const handleLastNameUpdate = (e) => {
        setActiveLastName(e.currentTarget.value)
    }

    return (
        <div 
            id="search" 
            className="container">
            {(() => {
                if(showErrors){
                    return <div 
                        id="searchValidation"
                        className="row mt-3 mb-0">
                        <div className="col">
                            <div className="alert alert-danger">
                                Please enter both a first name and a last name
                            </div>
                        </div>
                    </div> 
                }
            })()}
            <div className="row mt-2">
                <div className="col">
                    <form onSubmit={handleSearch}>
                        <div className="row">
                            <div className="col">
                                <label htmlFor="firstName">First name</label>
                                <input 
                                    id="firstName"
                                    className={`form-control ${showErrors ? 'is-invalid' : null}`}
                                    onChange={handleFirstNameUpdate}
                                    type="text">
                                </input>
                            </div>
                            <div className="col">
                                <label htmlFor="lastName">Last name</label>
                                <input
                                    id="lastName"
                                    className={`form-control ${showErrors ? 'is-invalid' : null}`}
                                    onChange={handleLastNameUpdate}
                                    type="text">
                                </input>
                            </div>
                        </div>
                        <button 
                            id="searchJokeBtn"
                            type="submit"
                            className="btn btn-primary mt-3">
                            Generate joke
                        </button>
                    </form>
                </div>
            </div>
            {(() => {
                if(activeSearchJoke){
                    return <div    
                        id="searchJokeText"
                        className="row mt-3">
                        <div className="col">
                            <div className="card">
                                <div className="card-body">
                                    {activeSearchJoke}
                                </div>
                            </div>
                        </div>
                    </div>
                }
            })()}
        </div>
    )
}