import React, { useState, useEffect } from 'react';
import axios from 'axios';

export default function NeverEnding() {

    const [jokeList, setJokeList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        fetchJokes()
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, []);

    useEffect(() => {
        if (!isLoading) return;
        fetchMoreJokes();
      }, [isLoading]);

    const handleScroll = () => {
        if (window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight) return;
        setIsLoading(true);
    }

    const fetchJokes = async () => {
        axios.get(`http://api.icndb.com/jokes/random/30?exclude=[explicit]&escape=javascript`)
        .then(res => {
            setJokeList(res.data.value)
            setTimeout(() => {
                setIsLoading(false)
            }, 2000);   
        })
    }

    const fetchMoreJokes = async () => {
        axios.get(`http://api.icndb.com/jokes/random/10?exclude=[explicit]&escape=javascript`)
        .then(res => {
            setJokeList(prevState => ([...prevState, ...res.data.value]))
            setTimeout(() => {
                setIsLoading(false)
            }, 2000); 
        })
    }
    
    if(jokeList){
        return (
            <div 
                id="neverEnding" 
                className="container">
                <div className="row mt-2">
                    <div className="col">
                        {jokeList.map((key, index) => {
                            return <p key={index}>{jokeList[index].joke}</p>
                        })}
                        {(() => {
                            if(isLoading){
                                return <div className="alert alert-info" role="alert">Loading more jokes...</div>
                            }
                        })()}
                    </div>
                </div>
            </div>
        )
    } else {
        return <p>Loading</p>
    }
}