import React, { useState } from 'react';
import axios from 'axios';
import { useHistory } from "react-router-dom";

export default function Home() {

    const history = useHistory();
    const [randomJoke, setRandomJoke] = useState(null);

    const handleRandomJokeClick = () => {
        axios.get(`http://api.icndb.com/jokes/random?exclude=[explicit]&escape=javascript`)
        .then(res => {
           setRandomJoke(res.data.value.joke)
        })
    }

    const handleSearchJokeClick = () => {
        history.push("/search");
    }

    const handleNeverEndingJokeClick = () => {
        history.push("/never-ending");
    }

    return (
        <div
            id="home" 
            className="container">
            <div className="row mt-3">
                <div className="home__btn-container col-12 col-md-4">
                    <button 
                        id="randomJokeBtn"
                        className="btn btn-block btn-primary" 
                        onClick={handleRandomJokeClick}>Generate random joke
                    </button>
                    {(() => {
                        if(randomJoke){
                            return <div 
                                id="randomJokeText"
                                className="row mt-3">
                                <div className="col"> 
                                    <div className="card">
                                        <div className="card-body">
                                            {randomJoke}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }
                    })()}
                </div>
                <div className="home__btn-container col-12 col-md-4">
                    <button 
                        className="btn btn-block btn-primary" 
                        onClick={handleSearchJokeClick}>Search joke
                    </button>
                </div>
                <div className="home__btn-container col-12 col-md-4">
                    <button 
                        className="btn btn-block btn-primary" 
                        onClick={handleNeverEndingJokeClick}>Never-ending jokes
                    </button>
                </div>
            </div>
        </div>
    )
}