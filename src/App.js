import React from 'react';
import './styles/styles.scss';
import Home from './components/Home';
import Search from './components/Search';
import NeverEnding from './components/NeverEnding';

import chuckNorrisIcon from './assets/chuck-norris-icon.png';

import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

export default function App() { 

    return (
        <Router>
            <nav className="navbar navbar-light bg-light">
                <a
                    className="navbar-brand" 
                    href="/">
                    <img 
                        alt="Chuck Norris"
                        className="chuck-norris-icon"
                        src={chuckNorrisIcon}/>
                        Chuck Norris Jokes
                </a>
            </nav>
            <main id="app">
                <Switch>
                    <Route exact path="/">
                        <Home/>
                    </Route>
                    <Route exact path="/search">
                        <Search/>
                    </Route>
                    <Route exact path="/never-ending">
                        <NeverEnding/>
                    </Route>
                </Switch>
            </main>
        </Router>
    )
}