describe('Search', function() {

    var homeRoute = `${Cypress.config().baseUrl}`;
    var apiRoute = `${Cypress.config().apiUrl}`;

    it('Does the search joke form validation work', function() {
        cy.visit(`${homeRoute}/search`)
        cy.get('#searchJokeBtn').click()
        cy.get('#searchJokeText').should('not.exist')
        cy.get('#searchValidation').should('exist')
    })
})