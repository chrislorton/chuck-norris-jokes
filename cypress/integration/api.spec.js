describe('API', function() {

    var homeRoute = `${Cypress.config().baseUrl}`;
    var apiRoute = `${Cypress.config().apiUrl}`;

    describe('Home', function() {
        it('Does the random joke button work', function() {
            cy.visit(homeRoute)
            cy.server();
            cy.route('GET', `${apiRoute}/jokes/random?exclude=[explicit]&escape=javascript`).as('randomJokeRoute');
            cy.get('#randomJokeBtn').click()
            cy.wait('@randomJokeRoute', { responseTimeout: 15000 });
            cy.get('#randomJokeText').should('exist')
        })
    })

    describe('Search', function() {
        it('Does the search joke button work', function() {
            cy.visit(`${homeRoute}/search`)
            cy.server();
            cy.route('GET', `${apiRoute}/jokes/random?exclude=[explicit]&escape=javascript&firstName=John&lastName=Doe`).as('searchJokeRoute');
            cy.get('#firstName').type('John')
            cy.get('#lastName').type('Doe')
            cy.get('#searchJokeBtn').click()
            cy.wait('@searchJokeRoute', { responseTimeout: 15000 });
            cy.get('#searchJokeText').should('exist')
        })
    })

    describe('Never Ending', function() {
        it('Does the never ending joke infinite scroll work', function() {
            cy.visit(`${homeRoute}/never-ending`)
            cy.server();
            cy.route('GET', `${apiRoute}/jokes/random/10?exclude=[explicit]&escape=javascript`).as('neverEndingJokeRoute');
            cy.scrollTo(0, 1000)
            cy.wait('@neverEndingJokeRoute', { responseTimeout: 15000 });
        })
    })
})