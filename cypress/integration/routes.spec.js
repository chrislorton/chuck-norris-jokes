describe('Routes', function() {

    var homeRoute = `${Cypress.config().baseUrl}`;

    describe('Home', function() {
        it('Does the home component exist', function() {
            cy.visit(homeRoute)
            cy.get('#home').should('exist')
        })
    })
    describe('Search', function() {
        it('Does the search component exist', function() {
            cy.visit(`${homeRoute}/search`)
            cy.get('#search').should('exist')
        })
    })
    describe('Never Ending', function() {
        it('Does the never ending component exist', function() {
            cy.visit(`${homeRoute}/never-ending`)
            cy.get('#neverEnding').should('exist')
        })
    })
})